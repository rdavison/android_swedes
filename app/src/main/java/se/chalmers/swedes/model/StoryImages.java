package se.chalmers.swedes.model;

import android.support.v4.util.SparseArrayCompat;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by richard on 13/12/15.
 */
public class StoryImages {

    private LinkedHashMap<String,List<String>> parallax;


    private SparseArrayCompat<SparseArrayCompat<ParallaxItem>> parallaxItems = new SparseArrayCompat<>(10);

    private int revision = -1;

    @SerializedName("main")
    private String mainUrl;
    private File mainImageFile;

    public String getMainUrl() {
        return mainUrl;
    }

    public LinkedHashMap<String, List<String>> getParallax() {
        return parallax;
    }

    public SparseArrayCompat<SparseArrayCompat<ParallaxItem>> getParallaxItems() {
        return parallaxItems;
    }

    public File getMainImageFile() {
        return mainImageFile;
    }

    public void setMainImageFile(File mainImageFile) {
        this.mainImageFile = mainImageFile;
    }
}
