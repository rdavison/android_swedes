package se.chalmers.swedes.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import se.chalmers.swedes.data.RestClient;

/**
 * Created by richard on 07/01/16.
 */
public class Category {

    private int id;
    private LinkedTreeMap<String,String> title;
    private List<ExplorationItem> items;
    private String imageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LinkedTreeMap<String, String> getTitle() {
        return title;
    }

    public List<ExplorationItem> getItems() {
        return items;
    }

    public String getItemSize() {
        return String.valueOf(items==null?0:items.size());
    }

    public String getImageUrl() {
        if(imageUrl==null){
            return imageUrl;
        }
        return String.format(RestClient.BASE_URL+"story/%d/images/%s",id,imageUrl);
    }


    /*
    public Category(){

    }

    public Category(Parcel other) {

        title = new LinkedTreeMap<>();
        if(other != null) {
            String[] keys = other.createStringArray();
            Bundle bundle = other.readBundle(String.class.getClassLoader());
            for(String key : keys)
                title.put(key, bundle.getString(key));
        }

        //other.readMap(title,LinkedHashMap.class.getClassLoader());
        other.readList(items, ExplorationItem.class.getClassLoader());
    }

    public static final Parcelable.Creator<Category> CREATOR
            = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };*/

}
