package se.chalmers.swedes.model;

import com.google.gson.internal.LinkedTreeMap;

import se.chalmers.swedes.data.RestClient;

/**
 * Created by richard on 02/01/16.
 */
public class VideoExplorationItem extends TextExplorationItem {

   private String videoUrl;

    public String getVideoUrl(int categoryId) {
        if(videoUrl==null){
            return videoUrl;
        }
        return String.format(RestClient.BASE_URL+"story/%d/videos/%s",categoryId,videoUrl);
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
