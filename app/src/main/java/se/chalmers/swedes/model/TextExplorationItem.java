package se.chalmers.swedes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

import java.util.LinkedHashMap;

import se.chalmers.swedes.data.RestClient;

/**
 * Created by richard on 02/01/16.
 */
public class TextExplorationItem extends ExplorationItem {

    @SerializedName("text")
    private LinkedTreeMap<String,String> text;
    private String imageUrl;

    private LinkedTreeMap<String,String> title;



    public LinkedTreeMap<String,String> getText() {
        return text;
    }

    public void setText(LinkedTreeMap<String,String> text) {
        this.text = text;
    }

    public LinkedTreeMap<String, String> getTitle() {
        return title;
    }

    public void setTitle(LinkedTreeMap<String, String> title) {
        this.title = title;
    }

    public String getImageUrl(int categoryId) {
        if(imageUrl==null){
            return imageUrl;
        }
        return String.format(RestClient.BASE_URL+"story/%d/images/%s",categoryId,imageUrl);
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /*
    public TextExplorationItem(Parcel in) {
        title = in.readString();
        in.readMap(text, LinkedHashMap.class.getClassLoader());
        imageUrl = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeMap(text);
        dest.writeString(imageUrl);
    }

    public static final Parcelable.Creator<TextExplorationItem> CREATOR
            = new Parcelable.Creator<TextExplorationItem>() {
        @Override
        public TextExplorationItem createFromParcel(Parcel in) {
            return new TextExplorationItem(in);
        }

        @Override
        public TextExplorationItem[] newArray(int size) {
            return new TextExplorationItem[size];
        }
    };*/
}
