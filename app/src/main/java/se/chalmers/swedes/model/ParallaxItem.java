package se.chalmers.swedes.model;

import java.io.File;

/**
 * Created by richard on 14/12/15.
 */
public class ParallaxItem {

    public float scale;
    public File file;
    public int offset;
    public int height;
    public int width;

    public ParallaxItem(File file, int offset, float scale, int width, int height) {
        this.file = file;
        this.offset = offset;
        this.scale = scale;
        this.height = height;
        this.width = width;
    }
}
