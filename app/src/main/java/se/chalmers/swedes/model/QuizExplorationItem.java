package se.chalmers.swedes.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by richard on 02/01/16.
 */
public class QuizExplorationItem extends ExplorationItem {


    private LinkedTreeMap<String,String> text;

    public LinkedTreeMap<String, String> getText() {
        return text;
    }

    public void setText(LinkedTreeMap<String, String> text) {
        this.text = text;
    }
}
