package se.chalmers.swedes.model;

import android.graphics.Color;

import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by richard on 12/12/15.
 */
public class Story {

    @SerializedName("bgColor")
    private String bgColorString;
    @SerializedName("fgColor")
    private String fgColorString;


    private transient Integer bgColor;
    private transient Integer fgColor;



    private int categoryId;

    private LinkedTreeMap<String,String> title;

    private LinkedTreeMap<String,String> description;

    private StoryImages images;


    public int getBgColor() {
        if(bgColor==null){
            bgColor = Color.parseColor(bgColorString);
        }
        return bgColor;
    }

    public int getFgColor() {
        if(fgColor==null){
            fgColor = Color.parseColor(fgColorString);
        }
        return fgColor;
    }

    public String getBgColorString() {
        return bgColorString;
    }

    public String getFgColorString() {
        return fgColorString;
    }

    public void setBgColorString(String bgColorString) {
        this.bgColorString = bgColorString;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }


    public StoryImages getImages() {
        return images;
    }

    public void setImages(StoryImages images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "ClassPojo [bgColorString = " + bgColorString + ", categoryId = " + categoryId + ", title = " + title + ", images = " + images + "]";
    }

    public LinkedTreeMap<String, String> getTitle() {
        return title;
    }

    public LinkedTreeMap<String, String> getDescription() {
        return description;
    }
}
