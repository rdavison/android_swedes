package se.chalmers.swedes.ui.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by richard on 09/12/15.
 */
public abstract class AnimatedView extends SurfaceView implements SurfaceHolder.Callback {


    private long lastUpdate = 0;
    private long sleepTime=0;

    private SurfaceHolder surfaceHolder;
    private Context context;

    private RenderThread thread;




    public AnimatedView(Context context) {
        this(context,null);
    }

    public AnimatedView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RenderThread getThread() {
        return thread;
    }



    public AnimatedView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        // create thread only; it's started in surfaceCreated()
        thread = new RenderThread(holder, context, new Handler() {
            @Override
            public void handleMessage(final Message message) {
                AnimatedView.this.handleMessage(message);
            }
        });



    }

    private void handleMessage(Message message) {

    }

    /* Callback invoked when the surface dimensions change. */
    @Override
    public void surfaceChanged(final SurfaceHolder holder, final int format, final int width,
                               final int height) {
        thread.setSurfaceSize(width, height);
    }

    /*
     * Callback invoked when the Surface has been created and is ready to be
     * used.
     */
    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        if (thread.isInBackground()) {
            thread.setInBackground(false);
        } else {
            thread.setRunning(true);
            thread.start();
        }
    }

    /*
     * Callback invoked when the Surface has been destroyed and must no longer
     * be touched. WARNING: after this method returns, the Surface/Canvas must
     * never be touched again!
     */
    @Override
    public void surfaceDestroyed(final SurfaceHolder holder) {
        thread.pause();
        thread.setInBackground(true);
    }

    class RenderThread extends Thread {


        private final Handler mHandler;
        private final Context mContext;

        private int mCanvasHeight = 1;
        private int mCanvasWidth = 1;

        private long mLastTime;
        private boolean mRun = false;
        private final SurfaceHolder mSurfaceHolder;
        private boolean mBackground = false;


        public RenderThread(SurfaceHolder holder, Context context, Handler handler) {

            mSurfaceHolder = surfaceHolder;
            mHandler = handler;
            mContext = context;

            final Resources res = context.getResources();
        }

        public void setSurfaceSize(int width, int height) {
            synchronized (mSurfaceHolder) {
                mCanvasWidth = width;
                mCanvasHeight = height;

                sizeChanged(width,height);

                //don't forget to resize the background image
                //do scale stuff
            }
        }

        public boolean isInBackground() {
            return mBackground;
        }

        public void setInBackground(boolean inBackground) {

            synchronized (mSurfaceHolder) {
                this.mBackground = inBackground;
            }
        }

        private void doStart() {
            synchronized (mSurfaceHolder) {
                //init
                AnimatedView.this.doStart();
            }
        }

        public synchronized void restoreState(final Bundle savedState) {
            synchronized (mSurfaceHolder) {

            }
        }

        @Override
        public void run() {
            while (mRun) {

                if (mBackground) {
                    try {
                        sleep(100);
                    } catch (final InterruptedException e) {
                    }
                } else {
                    Canvas c = null;
                    try {
                        c = mSurfaceHolder.lockCanvas(null);
                        synchronized (mSurfaceHolder) {
                            updatePhysics();
                            doDraw(c);
                        }
                    } finally {
                        if (c != null) {
                            mSurfaceHolder.unlockCanvasAndPost(c);
                        }
                    }
                }
            }
        }

        private void updatePhysics() {
            final long now = System.currentTimeMillis();

            // Do nothing if mLastTime is in the future.
            // This allows the game-start to delay the start of the physics
            // by 100ms or whatever.
            if (mLastTime > now) return;

            final double elapsed = (now - mLastTime) / 1000.0;

            AnimatedView.this.updatePhysics(elapsed);

            mLastTime = now;
        }


        public void setRunning(final boolean b) {
            mRun = b;
        }

        public Bundle saveState(final Bundle map) {
            synchronized (mSurfaceHolder) {
                if (map != null) {

                }
            }
            return map;
        }

        public void pause() {

        }
    }

    private void sizeChanged(int width, int height) {

    }

    protected void doStart(){

    }

    protected abstract void doDraw(Canvas c);

    protected abstract void updatePhysics(double elapsed);
}
