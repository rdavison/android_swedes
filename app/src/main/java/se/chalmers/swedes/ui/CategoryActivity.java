package se.chalmers.swedes.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import se.chalmers.swedes.R;
import se.chalmers.swedes.model.Category;

/**
 * Created by richard on 07/01/16.
 */
public class CategoryActivity extends BaseActivity {

    private static final String EXTRA_TITLE = "extra_title";
    private static final String EXTRA_CATEGORY_ID = "extra_category_id";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String title = getIntent().getExtras().getString(EXTRA_TITLE);
        int categoryId = getIntent().getExtras().getInt(EXTRA_CATEGORY_ID);

        setContentView(R.layout.activity_category);

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setSubtitle(R.string.select_category);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        if(savedInstanceState==null){
            getSupportFragmentManager().beginTransaction().add(R.id.cnt_fragment,new CategoryFragmentBuilder(categoryId).build()).commit();
        }


    }

    public static void startActivity(Context context, String title, int categoryId) {
        Intent intent = new Intent(context, CategoryActivity.class);
        intent.putExtra(EXTRA_TITLE,title);
        intent.putExtra(EXTRA_CATEGORY_ID,categoryId);
        context.startActivity(intent);
    }
}
