package se.chalmers.swedes.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;

import se.chalmers.swedes.R;

/**
 * Created by richard on 11/01/16.
 */
public class TextFloatingActionButton extends FloatingActionButton {


    private String text;
    private int textColor = Color.BLACK;
    private int textSize = 15;

    private Paint textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public TextFloatingActionButton(Context context) {
        this(context,null);
    }

    public TextFloatingActionButton(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public TextFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);



        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextFloatingActionButton,
                0, 0);


        Typeface tf =Typeface.createFromAsset(context.getAssets(),"fonts/Pacifico.ttf");
        textPaint.setTypeface(tf);

        try {

            text = a.getString(R.styleable.TextFloatingActionButton_text);
            textColor = a.getColor(R.styleable.TextFloatingActionButton_textColor, textColor);
            textSize = a.getDimensionPixelSize(R.styleable.TextFloatingActionButton_textSize,textSize);

        } finally {
            a.recycle();
            textPaint.setColor(textColor);
            textPaint.setTextSize(textSize);
        }


    }

    @Override
    protected void onDraw(Canvas canvas) {

        if(text!=null) {

            int xPos = (canvas.getWidth() / 2);
            int yPos = (int) ((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2));


            int textWidth = (int) textPaint.measureText(text);

            canvas.drawText(text, xPos - (textWidth / 2), yPos, textPaint);
        }

        super.onDraw(canvas);


    }

    public void setText(String text) {
        this.text = text;
        postInvalidate();
    }
}
