package se.chalmers.swedes.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.util.HashMap;
import java.util.Map;

import se.chalmers.swedes.R;
import se.chalmers.swedes.SwedesApplication;
import se.chalmers.swedes.model.Category;
import se.chalmers.swedes.util.Utils;

/**
 * Created by richard on 27/12/15.
 */
public class ExploreActivity extends BaseActivity {


    private static final String EXTRA_CATEGORY = "extra_category";
    private static final String EXTRA_CATEGORY_ID = "extra_category_id";
    private static final String EXTRA_TITLE = "extra_title";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        String json = getIntent().getExtras().getString(EXTRA_CATEGORY);
        int categoryId = getIntent().getExtras().getInt(EXTRA_CATEGORY_ID);
        HashMap<String,String> title = (HashMap<String, String>) getIntent().getExtras().getSerializable(EXTRA_TITLE);

        setContentView(R.layout.activity_explore);

        Toolbar toolbar = getToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setTitle(title.get(getLocale()));



        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) toolbar.getLayoutParams();
        params.topMargin = Utils.getStatusBarHeight(this);


        if(savedInstanceState==null){
            getSupportFragmentManager().beginTransaction().add(R.id.cnt_fragment,new ExploreFragmentBuilder(categoryId,json,title).build()).commit();
        }


    }
    public static void startActivity(Context context, int categoryId, Category category) {
        Intent intent = new Intent(context, ExploreActivity.class);
        intent.putExtra(EXTRA_CATEGORY_ID,categoryId);
        intent.putExtra(EXTRA_CATEGORY,new Gson().toJson(category));
        intent.putExtra(EXTRA_TITLE,category.getTitle());
        context.startActivity(intent);
    }
}
