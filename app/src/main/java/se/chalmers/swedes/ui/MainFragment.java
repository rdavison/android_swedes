package se.chalmers.swedes.ui;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import se.chalmers.swedes.R;
import se.chalmers.swedes.adapter.ParallaxAdapter;
import se.chalmers.swedes.adapter.StoryAdapter;
import se.chalmers.swedes.data.RestClient;
import se.chalmers.swedes.model.ParallaxItem;
import se.chalmers.swedes.model.Story;
import se.chalmers.swedes.ui.widget.ParallaxRecyclerView;
import se.chalmers.swedes.util.EventBus;
import se.chalmers.swedes.util.LUtils;
import se.chalmers.swedes.util.ReverseInterpolator;
import se.chalmers.swedes.util.StoryImageHandler;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends BaseFragment {

    private static final String TAG = MainFragment.class.getSimpleName();
    private static final float[] PARALLAX_SCROLL_FACTORS = new float[]{0.8f,0.6f,0.4f};

    private List<Story> stories;
    private ParallaxAdapter[] parallaxAdapters = new ParallaxAdapter[3];

    private float scale;

    private StoryAdapter adapter;
    private LinearLayoutManager layoutManager;


    private int backgroundTopColor;
    private int backgroundBottomColor;
    private View root;

    private RestClient client = RestClient.getInstance();
    private StoryImageHandler storyImageHandler;

    private EventBus<StoryImageHandler.DownloadProgress> progressEventBus = EventBus.createWithLatest();

    @Bind(R.id.view_background)
    View backgroundView;

    @Bind(R.id.view_foreground)
    View foregroundView;

    @Bind(R.id.cnt_loading)
    View loadingBackgroundView;

    @Bind(R.id.img_mascot)
    ImageView imgMascot;

    @Bind(R.id.lst_main)
    RecyclerView recyclerView;

    @Bind(R.id.prg_main)
    ProgressBar progressBar;

    @Bind(R.id.txt_loading)
    TextView txtLoading;

    @Bind({R.id.lst_paralax_0,R.id.lst_paralax_1,R.id.lst_paralax_2})
    ParallaxRecyclerView[] parallaxRecyclerViews;



    private ObjectAnimator backgroundAnimator;//= ObjectAnimator.ofObject(backgroundView,"backgroundColor",new ArgbEvaluator(),0,1);
    private ObjectAnimator foregroundAnimator;// = ObjectAnimator.ofObject(foregroundView,"backgroundColor",new ArgbEvaluator(),0,1);

    private int lastFgColor;
    private int lastBgColor;

    private AnimationDrawable mooseAnimationDrawable;
    private Drawable mooseDrawable;
    private Interpolator fastOutSlowInInterpolator;
    private ReverseInterpolator reverseFastOutSlowInInterpolator;
    private List[] parallaxItemArray;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        try {
            //noinspection ResourceType
            mooseAnimationDrawable = (AnimationDrawable) Drawable.createFromXml(getResources(), getResources().getXml(R.drawable.moose_animation));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        mooseDrawable = ContextCompat.getDrawable(getContext(),R.drawable.moose);

        storyImageHandler = new StoryImageHandler(client,getContext().getCacheDir(),progressEventBus);

        scale = getContext().getResources().getDisplayMetrics().density/StoryImageHandler.IMAGE_DENSITY;

        fastOutSlowInInterpolator = LUtils.loadInterpolatorWithFallback(getContext(), android.R.interpolator.fast_out_slow_in, android.R.interpolator.decelerate_cubic);
        reverseFastOutSlowInInterpolator = new ReverseInterpolator(fastOutSlowInInterpolator);


        loadData();
    }

    private void loadData() {
        client.startStories().flatMap(new Func1<List<Story>, Observable<Void>>() {
            @Override
            public Observable<Void> call(List<Story> stories) {

                MainFragment.this.stories = stories;

                return storyImageHandler.handleImages(stories,scale);

            }
        }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<Void>() {
            @Override
            public void call(Void nothing) {


                parallaxItemArray = new List[parallaxAdapters.length];



                for(Story story : stories){

                    SparseArrayCompat<SparseArrayCompat<ParallaxItem>> innerItems = story.getImages().getParallaxItems();
                    for(int ii = 0; ii < innerItems.size(); ii++){
                        if(parallaxItemArray[ii] == null){
                            parallaxItemArray[ii] = new ArrayList<>();
                        }
                        int parentKey = innerItems.keyAt(ii);
                        SparseArrayCompat<ParallaxItem> childItems = innerItems.get(parentKey);
                        for(int iii = 0; iii < childItems.size(); iii++){
                            int key = childItems.keyAt(iii);
                            parallaxItemArray[parentKey].add(childItems.get(key));
                        }

                    }
                }


                adapter = new StoryAdapter(getContext(),stories);
                adapter.setItemClickListener(new StoryAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(int position) {
                        Story story = stories.get(position);
                        CategoryActivity.startActivity(getContext(),story.getTitle().get(getLocale()),story.getCategoryId());

                    }
                });
                addAdapters();

                new LUtils.CircularRevealBuilder(loadingBackgroundView).withLayer().duration(400).interpolator(reverseFastOutSlowInInterpolator).listener(new LUtils.CircularRevealListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        loadingBackgroundView.setVisibility(View.GONE);
                    }
                }).createAndStart();

            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
                Toast.makeText(getContext(),"ERROR",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addAdapters() {

        if(stories!=null) {

            Story firstStory = stories.get(0);


            lastBgColor = firstStory.getBgColor();
            lastFgColor = firstStory.getFgColor();

            backgroundView.setBackgroundColor(lastBgColor);
            foregroundView.setBackgroundColor(lastFgColor);

            int i = 0;
            for (ParallaxAdapter adapter : parallaxAdapters) {
                adapter.setItems(parallaxItemArray[i]);
                adapter.notifyDataSetChanged();
                i++;
            }
            recyclerView.setAdapter(adapter);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_main, container, false);

        ButterKnife.bind(this,root);

        progressBar.setMax(100);
        txtLoading.setText(R.string.loading);


        RecyclerView.OnItemTouchListener disabler = new RecyclerViewDisabler();

        int i = 0;
        for(ParallaxRecyclerView rv : parallaxRecyclerViews){
            rv.addOnItemTouchListener(disabler);
            rv.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false));
            parallaxAdapters[i] = new ParallaxAdapter(getContext(),scale,PARALLAX_SCROLL_FACTORS[i]);
            rv.setAdapter(parallaxAdapters[i]);
            i++;
        }

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            public boolean changeBackgroundOnScrolled = false;
            private int totalScrolled = 0;
            private boolean scrollingUp;
            private int lastDx = 0;


            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                switch (newState){
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        changeBackgroundOnScrolled = false;
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        changeBackgroundOnScrolled = true;
                        imgMascot.setImageDrawable(mooseAnimationDrawable);
                        mooseAnimationDrawable.start();
                        break;
                    case RecyclerView.SCROLL_STATE_IDLE:
                        //recyclerView.smoothScrollToPosition(layoutManager.findFirstVisibleItemPosition());
                        mooseAnimationDrawable.stop();
                        imgMascot.setImageDrawable(mooseDrawable);
                        updateBackgroundColor();
                        break;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                scrollingUp = dx < 0;
                totalScrolled += dx;

                if(dx < 0){
                    imgMascot.setScaleX(-1);
                }else{
                    imgMascot.setScaleX(1);
                }

                if(changeBackgroundOnScrolled) {
                    updateBackgroundColor();
                }

                if((dx > 0 && dx > lastDx) || (dx < 0 && dx < lastDx)){
                    lastDx = dx;
                    //mascot.setTranslationX(lastDx);
                }


                

                for(int i = 0; i < parallaxAdapters.length; i++){

                    float parallaxFactor = parallaxAdapters[i].getParallaxScrollFactor();
                    float adjustedScrollFactor = ((parallaxFactor*stories.size())-1)/((stories.size())-1);
                    parallaxRecyclerViews[i].scrollHorizontallyBy((int)(dx*adjustedScrollFactor));

                }
            }

            private void updateBackgroundColor() {

                int width = adapter.getWidth();

                int firstPosition = totalScrolled%width<width/2 ? layoutManager.findFirstVisibleItemPosition()
                        : layoutManager.findLastVisibleItemPosition();


                if(firstPosition<0){
                    firstPosition = 0;
                }



                //int firstPosition = layoutManager.findFirstVisibleItemPosition();
                Story currentStory = stories.get(firstPosition);

                int bgColor = currentStory.getBgColor();
                int fgColor = currentStory.getFgColor();
                if(bgColor != lastBgColor){
                    backgroundAnimator = ObjectAnimator.ofObject(backgroundView,"backgroundColor",new ArgbEvaluator(),lastBgColor,bgColor);
                    lastBgColor = bgColor;
                    backgroundAnimator.start();
                }
                if(fgColor != lastFgColor){
                    foregroundAnimator = ObjectAnimator.ofObject(foregroundView,"backgroundColor",new ArgbEvaluator(),lastFgColor,fgColor);
                    lastFgColor = fgColor;
                    foregroundAnimator.start();
                }
            }
        });

        progressEventBus.observe().subscribeOn(Schedulers.io()).onBackpressureLatest()
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<StoryImageHandler.DownloadProgress>() {
            @Override
            public void call(StoryImageHandler.DownloadProgress downloadProgress) {
                Log.d(TAG, "Download progress!! = " + downloadProgress.progress);
                progressBar.setProgress(downloadProgress.progress);
                txtLoading.setText(getString(R.string.downloading_images,downloadProgress.progress));
            }
        });

        if(savedInstanceState==null) {

        }else{
            if(stories!=null) {
                loadingBackgroundView.setVisibility(View.GONE);
            }
            addAdapters();
        }





        return root;
    }

    public static class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            return true;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
