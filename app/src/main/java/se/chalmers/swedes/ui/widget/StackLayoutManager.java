package se.chalmers.swedes.ui.widget;

import android.graphics.PointF;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.DecelerateInterpolator;


/**
 * Created by richard on 02/01/16.
 */
public class StackLayoutManager extends RecyclerView.LayoutManager implements View.OnTouchListener {

    private static final String TAG = StackLayoutManager.class.getSimpleName();

    private static final int DP_OFFSET_PER_CARD = 8;
    private static final int ANIMATION_SPEED_SWIPE = 400;
    private static final int ANIMATION_SPEED_RETURN = 200;


    private int stackCount;
    private int currentPosition = 0;

    private int dpCardOffset;
    private PointF cardStartPos = new PointF();
    private PointF cardDeltaPos = new PointF();
    private boolean ignoreMotionEvent;
    private int layerType;
    private int maximumVelocity;
    private int minimumVelocity;

    private VelocityTracker velocityTracker;
    private float velocityY;
    private float velocityX;
    private DecelerateInterpolator decelerateInterpolator;
    private RecyclerView.Recycler recycler;
    private RecyclerView recyclerView;
    private boolean forward = true;

    private OnLayoutChildrenListener onLayoutChildrenListener;

    private interface OnLayoutChildrenListener{
        void onLayoutChildrenStarted();
        void onLayoutChildrenComplete();
    }

    private SavedState pendingSavedState = null;




    public StackLayoutManager(int stackCount) {

        //add one view behind
        this.stackCount = stackCount;


    }

    @Override
    public void onAttachedToWindow(RecyclerView view) {
        super.onAttachedToWindow(view);

        float density = view.getContext().getResources().getDisplayMetrics().density;
        dpCardOffset = (int) (density * DP_OFFSET_PER_CARD);


        ViewConfiguration configuration = ViewConfiguration.get(view.getContext());

        maximumVelocity = configuration.getScaledMaximumFlingVelocity();
        minimumVelocity = configuration.getScaledMinimumFlingVelocity();

        this.recyclerView = view;


    }

    //onAttachedToWindow(RecyclerView view)

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public View findViewByPosition(int position) {
        final int childCount = getChildCount();
        if (childCount == 0) {
            return null;
        }
        final int firstChild = getPosition(getChildAt(0));
        final int viewPosition = position - firstChild;
        if (viewPosition >= 0 && viewPosition < childCount) {
            return getChildAt(viewPosition);
        }
        return null;
    }

    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                       int position) {
        LinearSmoothScroller linearSmoothScroller =
                new LinearSmoothScroller(recyclerView.getContext()) {
                    @Override
                    public PointF computeScrollVectorForPosition(int targetPosition) {
                        return StackLayoutManager.this
                                .computeScrollVectorForPosition(targetPosition);
                    }
                };
        linearSmoothScroller.setTargetPosition(position);
        startSmoothScroll(linearSmoothScroller);
    }

    public PointF computeScrollVectorForPosition(int targetPosition) {
        if (getChildCount() == 0) {
            return null;
        }
        final int firstChildPos = getPosition(getChildAt(0));
        final int direction = targetPosition < firstChildPos ? -1 : 1;
        return new PointF(0, direction);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {

        if(onLayoutChildrenListener!=null){
            onLayoutChildrenListener.onLayoutChildrenStarted();
        }

        if (pendingSavedState != null){
            currentPosition = pendingSavedState.position;
            pendingSavedState = null;
        }

        this.recycler = recycler;

        if (getItemCount() == 0) {
            detachAndScrapAttachedViews(recycler);
            return;
        }
        if (getChildCount() == 0 && state.isPreLayout()) {
            //Nothing to do during prelayout when empty
            return;
        }

        if (state.isPreLayout()) {
            return;
        }


        fillVisibleChildren(recycler);

        if(onLayoutChildrenListener!=null){
            onLayoutChildrenListener.onLayoutChildrenComplete();
        }
    }

    @Override
    public void onItemsChanged(RecyclerView recyclerView) {
        //onLayoutChildren()

    }


    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof SavedState) {
            pendingSavedState = (SavedState) state;
            requestLayout();
        }
    }

    private void fillVisibleChildren(RecyclerView.Recycler recycler) {




        while(getChildCount()<(stackCount+1)){


            int indexToRender = forward?currentPosition+getChildCount():currentPosition;

            int debugChildCount = getChildCount();

            int diff = getItemCount()-currentPosition;

            if(indexToRender >= getItemCount() || (!forward && diff < getChildCount()+1) && diff <= stackCount){
                break;
            }

            View view = recycler.getViewForPosition(indexToRender);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int translationZ = forward?(stackCount-(getChildCount()+1)):getChildCount();
                view.setTranslationZ(translationZ);
            }

            view.setX(0);
            view.setY(0);
            view.setRotation(0);

            addView(view,forward?0:getChildCount());

            //view.setAlpha(0);
            //view.animate().alpha(1).setDuration(ANIMATION_SPEED_RETURN).start();
            layoutView(view, forward?getChildCount()-1:stackCount-getChildCount());
            recyclerView.onChildAttachedToWindow(view);
        }




        View topView = getTopView();
        if(topView!=null) {
            topView.setOnTouchListener(this);
        }




        //recyclerView.postInvalidate();


    }

    private void layoutView(View view, int index) {
        if(index == stackCount){
            index--;
        }
        measureChildWithMargins(view, 0, 0);
        int left = getDecoratedLeft(view) + getPaddingLeft();
        int top = getDecoratedTop(view) + getPaddingTop() + (dpCardOffset * index);
        int width = getDecoratedMeasuredWidth(view);
        int height = getDecoratedMeasuredHeight(view) - ((stackCount-1) * dpCardOffset);
        layoutDecorated(view, left, top,
                left + width,
                top + height);
    }

    @Override
    public void onAdapterChanged(RecyclerView.Adapter oldAdapter, RecyclerView.Adapter newAdapter) {
        //Completely scrap the existing layout
        removeAllViews();
    }


    @Override
    public boolean canScrollHorizontally() {
        return false;
    }

    @Override
    public boolean canScrollVertically() {
        return false;
    }

    @Override
    public boolean onTouch(final View view, MotionEvent event) {

        int index = event.getActionIndex();
        int pointerId = event.getPointerId(index);


        if (ignoreMotionEvent) {
            return false;
        }

        switch (event.getActionMasked()) {

            case MotionEvent.ACTION_DOWN:

                if(velocityTracker == null) {
                    // Retrieve a new VelocityTracker object to watch the velocity of a motion.
                    velocityTracker = VelocityTracker.obtain();
                }
                else {
                    // Reset the velocity tracker back to its initial state.
                    velocityTracker.clear();
                }
                // Add a user's movement to the tracker.
                velocityTracker.addMovement(event);

                cardStartPos.x = view.getX();
                cardStartPos.y = view.getY();
                cardDeltaPos.x = view.getX() - event.getRawX();
                cardDeltaPos.y = view.getY() - event.getRawY();
                layerType = view.getLayerType();
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                break;

            case MotionEvent.ACTION_MOVE:

                float targetY = event.getRawY() + cardDeltaPos.y;
                float targetX = event.getRawX() + cardDeltaPos.x;

                if (targetY > cardStartPos.y) {
                    targetY = cardStartPos.y;
                }

                velocityTracker.addMovement(event);
                velocityTracker.computeCurrentVelocity(1000);

                velocityY = velocityTracker.getYVelocity(pointerId);
                velocityX = velocityTracker.getXVelocity(pointerId);


                float deltaX = targetX - cardStartPos.x;
                float deltaY = cardStartPos.y + (view.getHeight() / 2) - targetY;

                float targetRotation = 90 - (float) Math.toDegrees(Math.atan((deltaY / deltaX)));

                if (targetRotation > 90) {
                    targetRotation -= 180;
                }


                view.setX(targetX);
                view.setY(targetY);
                view.setRotation(targetRotation);


                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:

                velocityTracker.recycle();
                velocityTracker = null;

                ignoreMotionEvent = true;

                float swipeY = cardStartPos.y - (event.getRawY() + cardDeltaPos.y);

                Log.d(TAG, "VELOCITY Y = " + velocityY);
                Log.d(TAG, "SWIPE Y = " + swipeY);


                float localY = event.getY();
                int halfViewHeight = view.getHeight()/2;

                if(swipeY == 0){
                    if(localY <= halfViewHeight){
                        showPrevious();
                    }else{
                        showNext();
                    }
                }else{
                    if (velocityY < -minimumVelocity || swipeY > (view.getHeight() / 4) || view.getRotation() > 30) {
                        showNext();
                    }else if(swipeY <= 0 && velocityY > minimumVelocity && currentPosition>0){
                        showPrevious();
                    }else {
                        decelerateInterpolator = new DecelerateInterpolator();
                        view.animate().x(cardStartPos.x).y(cardStartPos.y).rotation(0).setDuration(ANIMATION_SPEED_RETURN).setInterpolator(decelerateInterpolator).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                view.setLayerType(layerType, null);
                                ignoreMotionEvent = false;
                            }
                        }).start();
                    }
                }

                velocityY = 0;
                velocityX = 0;

            default:
                return false;
        }
        return true;

    }

    private void showPrevious(){
        if(currentPosition>0) {

            Log.d(TAG, "SHOWING PREV!");

            boolean callFillChildren = false;

            currentPosition--;
            forward = false;

            View lastTopView = getTopView();
            lastTopView.setOnTouchListener(null);

            if(getChildCount() == stackCount+1){
                removeAndRecycleView(getChildAt(0),recycler);
            }else{
                callFillChildren = true;
            }


            onLayoutChildrenListener = new OnLayoutChildrenListener() {
                @Override
                public void onLayoutChildrenStarted() {

                }

                @Override
                public void onLayoutChildrenComplete() {
                    onLayoutChildrenListener = null;
                    View topView = getTopView();
                    topView.setY(-topView.getHeight());

                    animateNextOrPrev(cardStartPos.x,cardStartPos.y);
                }
            };

            if(callFillChildren){
                fillVisibleChildren(recycler);
            }
        }else {
            ignoreMotionEvent = false;
        }

    }

    @Override
    public boolean supportsPredictiveItemAnimations() {
        return false;
    }

    private void showNext() {

        Log.d(TAG, "SHOWING NEXT!");

        forward = true;

        currentPosition++;
        View topView = getTopView();
        topView.setOnTouchListener(null);

        if (velocityY < minimumVelocity) {
            velocityY = (minimumVelocity*3);
        }

        velocityY *= 7;

        animateNextOrPrev(topView.getX()-(-velocityX),topView.getY()-velocityY);
    }

    private View getTopView() {
        return getChildAt(getChildCount() - 1);
    }


    private void animateNextOrPrev(float x, float y) {

        int loop = 0;
        for (int i = getChildCount() - 1; i >= 0; i--) {
            View view = getChildAt(i);
            if (loop == 0) {
                final View topView = getTopView();
                ViewPropertyAnimator animator = topView.animate().x(x).y(y).setDuration(ANIMATION_SPEED_SWIPE).setInterpolator(decelerateInterpolator).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        topView.setLayerType(layerType, null);
                        ignoreMotionEvent = false;
                        if(forward) {
                            removeAndRecycleView(topView, recycler);
                        }
                    }
                });
                if(forward){
                    animator.rotation(0);
                }
                animator.start();
            } else if(i > 0 || getChildCount() <= stackCount){
                //int offset = forward?-1:1;
                ViewCompat.animate(view).x(cardStartPos.x).withLayer().rotation(0).translationZ(i-(forward?-1:1)).yBy(dpCardOffset*(forward?-1:1)).setInterpolator(decelerateInterpolator).setDuration(ANIMATION_SPEED_RETURN).start();
            }
            loop++;
        }
    }

    @Override
    public Parcelable onSaveInstanceState() {
        if (pendingSavedState != null) {
            return new SavedState(pendingSavedState);
        }
        SavedState state = new SavedState();
        if (getChildCount() > 0) {
            state.position = currentPosition;
        }
        return state;
    }

    private static class SavedState implements Parcelable {

        private int position;

        public SavedState() {

        }

        SavedState(Parcel in) {
            position = in.readInt();
        }

        public SavedState(SavedState other) {
            position = other.position;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(position);
        }

        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }
}
