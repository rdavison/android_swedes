package se.chalmers.swedes.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.StackView;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.fragmentargs.bundler.ArgsBundler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.chalmers.swedes.R;
import se.chalmers.swedes.adapter.ExploreAdapter;
import se.chalmers.swedes.data.RestClient;
import se.chalmers.swedes.model.Category;
import se.chalmers.swedes.model.ExplorationItem;
import se.chalmers.swedes.model.TextExplorationItem;
import se.chalmers.swedes.ui.widget.StackLayoutManager;
import se.chalmers.swedes.util.Utils;

/**
 * Created by richard on 27/12/15.
 */
@FragmentWithArgs
public class ExploreFragment extends BaseFragment {


    private Category category;

    public static class SerializableArgsBundler implements ArgsBundler<HashMap<String,String>>{


        @Override
        public HashMap<String,String> get(String key, Bundle bundle) {
            return (HashMap<String, String>) bundle.getSerializable(key);
        }

        @Override
        public void put(String key, HashMap<String,String> value, Bundle bundle) {
            bundle.putSerializable(key,value);
        }
    }

    @Arg
    int categoryId;

    @Arg
    String categoryJson;

    @Bind(R.id.lst_explore)
    RecyclerView recyclerView;

    @Bind(R.id.txt_complete)
    TextView txtComplete;


    @Bind(R.id.txt_awesome)
    TextView txtAwesome;


    @Arg(bundler = SerializableArgsBundler.class)
    HashMap<String,String> title;

    private ExploreAdapter adapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentArgs.inject(this);



        category = RestClient.getInstance().getGson().fromJson(categoryJson,Category.class);

        adapter = new ExploreAdapter(getContext(),category.getTitle(),categoryId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_explore,container,false);

        ButterKnife.bind(this,root);

        recyclerView.setPadding(recyclerView.getPaddingLeft(),recyclerView.getPaddingTop()+ Utils.getActionBarHeight(getContext())+Utils.getStatusBarHeight(getContext()),recyclerView.getPaddingRight(),recyclerView.getPaddingBottom());
        recyclerView.setLayoutManager(new StackLayoutManager(3));
        recyclerView.setAdapter(adapter);



        txtComplete.setText(getString(R.string.item_completed,category.getTitle().get(getLocale())));
        String[] awesome = getResources().getStringArray(R.array.awesome);

        txtAwesome.setText(awesome[(int) (Math.random()*awesome.length)]);


        adapter.setItems(category.getItems());

        return root;
    }
}
