package se.chalmers.swedes.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.Locale;

import se.chalmers.swedes.R;
import se.chalmers.swedes.SwedesApplication;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by richard on 22/12/15.
 */
public class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private String locale;

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

        this.locale = SwedesApplication.getLocale();

        final Configuration override = new Configuration(
                // Copy the original configuration so it isn't lost.
                newBase.getResources().getConfiguration()
        );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            override.setLocale(new Locale(locale));
        }else{
            override.locale = new Locale(locale);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            applyOverrideConfiguration(override);
        }else{
            Resources res = newBase.getResources();
            res.updateConfiguration(override, res.getDisplayMetrics());
        }

    }

    public String getLocale() {
        return locale;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        ensureToolbar();
    }

    private void ensureToolbar() {
        toolbar = (Toolbar)findViewById(R.id.toolbar);

        if(toolbar!=null){
            setSupportActionBar(toolbar);
        }

    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);

        ensureToolbar();
    }
}
