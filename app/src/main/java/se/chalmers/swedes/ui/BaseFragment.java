package se.chalmers.swedes.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * Created by richard on 05/01/16.
 */
public class BaseFragment extends Fragment {


    private String locale;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof BaseActivity){
            this.locale = ((BaseActivity)context).getLocale();
        }
    }

    public String getLocale() {
        return locale;
    }
}
