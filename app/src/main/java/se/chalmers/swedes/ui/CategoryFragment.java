package se.chalmers.swedes.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import se.chalmers.swedes.R;
import se.chalmers.swedes.adapter.CategoryAdapter;
import se.chalmers.swedes.data.RestClient;
import se.chalmers.swedes.model.Category;

/**
 * Created by richard on 07/01/16.
 */
@FragmentWithArgs
public class CategoryFragment extends BaseFragment {

    private RestClient client = RestClient.getInstance();


    @Arg
    int categoryId;

    @Bind(R.id.lst_category)
    RecyclerView recyclerView;

    @Bind(R.id.btn_retry)
    Button btnRetry;

    @Bind(R.id.prg_category)
    ProgressBar progressBar;
    private CategoryAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentArgs.inject(this);

        client.categories(categoryId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<List<Category>>() {
            @Override
            public void call(final List<Category> categories) {

                progressBar.setVisibility(View.GONE);
                adapter = new CategoryAdapter(getContext(),categories);
                adapter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ExploreActivity.startActivity(getContext(),categoryId,categories.get(position));
                    }
                });

                recyclerView.setAdapter(adapter);

            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();
                btnRetry.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_category,container,false);
        ButterKnife.bind(this,root);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        return root;
    }
}
