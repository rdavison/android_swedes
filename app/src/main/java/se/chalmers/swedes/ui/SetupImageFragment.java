package se.chalmers.swedes.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.chalmers.swedes.R;

/**
 * Created by richard on 19/12/15.
 */
@FragmentWithArgs
public class SetupImageFragment extends SetupFragment {

    @Arg
    int title;
    @Arg
    int description;
    @Arg
    int imageOne;
    @Arg
    int imageTwo;

    @Bind(R.id.img_one)
    ImageView imgOne;

    @Bind(R.id.img_two)
    ImageView imgTwo;

    @Bind(R.id.txt_title)
    TextView txtTitle;

    @Bind(R.id.txt_description)
    TextView txtDescription;

    @Override
    public int layoutTopResource() {
        return R.layout.view_setup_image;
    }

    @Override
    public int layoutBottomResource() {
        return R.layout.view_setup_text;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root =  super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, root);

        bindView();

        return root;
    }

    @Override
    public void bindView() {

        imgOne.setImageResource(imageOne);
        imgTwo.setImageResource(imageTwo);

        txtTitle.setText(title);
        txtDescription.setText(description);
    }

    public void onScrolled(float positionOffset, int positionOffsetPixels){
        int offset = (int) (-positionOffsetPixels*0.5);
        if(imgOne !=null && imgTwo != null) {
            imgOne.setTranslationX(offset);
            imgTwo.setTranslationX((float) (offset * 0.6));
        }
    }
}
