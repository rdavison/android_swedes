package se.chalmers.swedes.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by richard on 12/12/15.
 */
public class ImageParalaxView extends View {



    public ImageParalaxView(Context context) {
        this(context,null);
    }

    public ImageParalaxView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ImageParalaxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
