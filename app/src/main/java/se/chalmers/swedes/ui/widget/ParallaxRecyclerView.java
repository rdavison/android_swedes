package se.chalmers.swedes.ui.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import java.lang.reflect.Field;

/**
 * Created by richard on 12/12/15.
 */
public class ParallaxRecyclerView extends RecyclerView {


    private State state;
    private Recycler recycler;

    public ParallaxRecyclerView(Context context) {
        this(context,null);
    }

    public ParallaxRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ParallaxRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        Field fieldState = null;
        Field fieldRecycler = null;
        try {
            fieldState = this.getClass().getSuperclass().getDeclaredField("mState");
            fieldState.setAccessible(true);
            state = (State) fieldState.get(this);

            fieldRecycler = this.getClass().getSuperclass().getDeclaredField("mRecycler");
            fieldRecycler.setAccessible(true);
            recycler = (Recycler)fieldRecycler.get(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void scrollHorizontallyBy(int dx){
        getLayoutManager().scrollHorizontallyBy(dx,recycler,state);
    }
}
