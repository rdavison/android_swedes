package se.chalmers.swedes.ui;

import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.hannesdorfmann.fragmentargs.FragmentArgs;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.chalmers.swedes.SwedesApplication;
import se.chalmers.swedes.R;

/**
 * Created by richard on 21/12/15.
 */
@FragmentWithArgs(inherited = false)
public class SetupSelectLanguageFragment extends SetupFragment {


    @Bind(R.id.pkr_language)
    NumberPicker pkrLanguage;

    @Bind(R.id.txt_speech_bubble)
    TextView txtSpeechBubble;

    @Bind(R.id.img_one)
    ImageView imgOne;

    @Bind(R.id.img_two)
    ImageView imgTwo;

    @Override
    public int layoutTopResource() {
        return R.layout.view_setup_mascot;
    }

    @Override
    public int layoutBottomResource() {
        return R.layout.view_setup_language;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        ButterKnife.bind(this,root);

        String[] tempLang = getResources().getStringArray(R.array.languages);
        final LinkedHashMap<String,String> languages = new LinkedHashMap<>(tempLang.length);

        String defaultLanguage = SwedesApplication.getLocale();

        int i = 0, selectedIndex = 0;
        for(String lang : tempLang){
            String[] langIso = lang.split(",");
            if(langIso[1].equals(defaultLanguage)){
                selectedIndex = i;
            }
            languages.put(langIso[1],langIso[0]);
            i++;
        }

        setNumberPickerTextColor(pkrLanguage,0xFFFFFFFF,0xFFFFFFFF);

        pkrLanguage.setMinValue(0);
        pkrLanguage.setMaxValue(tempLang.length-1);
        pkrLanguage.setDisplayedValues(languages.values().toArray(new String[tempLang.length]));
        pkrLanguage.setValue(selectedIndex);
        pkrLanguage.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                String languageCode = languages.keySet().toArray(new String[languages.size()])[newVal];
                ((SetupActivity)getActivity()).onEvent(languageCode);
                SwedesApplication.setLocale(languageCode);
            }
        });

        bindView();

        return root;
    }

    @Override
    public void bindView() {
        txtSpeechBubble.setText(R.string.welcome);
    }

    @Override
    public void onScrolled(float positionOffset, int positionOffsetPixels){
        int offset = (int) (-positionOffsetPixels*0.5);
        if(isAdded()) {
            txtSpeechBubble.setTranslationX(offset);
            imgOne.setTranslationX(offset);
            imgTwo.setTranslationX((float) (offset * 0.6));
        }
    }

    public static void setNumberPickerTextColor(NumberPicker numberPicker, int textColor,int dividerColor)
    {

        final int count = numberPicker.getChildCount();
        try{
            for(int i = 0; i < count; i++) {
                View child = numberPicker.getChildAt(i);
                if (child instanceof EditText) {

                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint) selectorWheelPaintField.get(numberPicker)).setColor(textColor);
                    ((EditText) child).setTextColor(textColor);
                    numberPicker.invalidate();

                }
            }
            Field[] pickerFields = NumberPicker.class.getDeclaredFields();
            for (java.lang.reflect.Field pf : pickerFields) {
                if (pf.getName().equals("mSelectionDivider")) {
                    pf.setAccessible(true);
                    ColorDrawable colorDrawable = new ColorDrawable(dividerColor);
                    pf.set(numberPicker, colorDrawable);
                    break;
                }
            }

        }
        catch(NoSuchFieldException e){
            e.printStackTrace();
        }
        catch(IllegalAccessException e){
            e.printStackTrace();
        }
        catch(IllegalArgumentException e){
            e.printStackTrace();
        }
    }
}
