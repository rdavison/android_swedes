package se.chalmers.swedes.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import se.chalmers.swedes.R;
import se.chalmers.swedes.ui.widget.CirclePageIndicator;
import se.chalmers.swedes.util.Preferences;
import se.chalmers.swedes.util.SimpleFragmentPagerAdapter;

/**
 * Created by richard on 19/12/15.
 */
public class SetupActivity extends BaseActivity {

    public static final int REQ_SETUP = 0;


    @Bind(R.id.txt_done)
    TextView txtDone;
    @Bind(R.id.txt_skip)
    TextView txtSkip;

    @Bind(R.id.view_switcher)
    ViewSwitcher viewSwitcher;


    @Bind(R.id.pager)
    ViewPager viewPager;
    @Bind(R.id.pager_indicator)
    CirclePageIndicator circlePageIndicator;


    private String strDone;
    private SimpleFragmentPagerAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setup);
        ButterKnife.bind(this);

        strDone = getString(R.string.done);

        txtSkip.setVisibility(View.INVISIBLE);



        final List<Fragment> fragments = new ArrayList<>();
        fragments.add(new SetupSelectLanguageFragmentBuilder().build());
        fragments.add(new SetupImageFragmentBuilder(R.string.intro_desc_1,R.drawable.intro_0_1,R.drawable.intro_placeholder,R.string.intro_title_1).build());
        fragments.add(new SetupImageFragmentBuilder(R.string.intro_desc_2,R.drawable.intro_1_1,R.drawable.intro_placeholder,R.string.intro_title_2).build());
        fragments.add(new SetupImageFragmentBuilder(R.string.intro_desc_3,R.drawable.intro_2_1,R.drawable.intro_placeholder,R.string.intro_title_3).build());


        adapter = new SimpleFragmentPagerAdapter(getSupportFragmentManager(),fragments);

        final int animationDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        final Animation fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        fadeIn.setDuration(animationDuration);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        Animation fadeout = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        fadeout.setDuration(animationDuration);
        fadeout.setInterpolator(new AccelerateDecelerateInterpolator());

        viewSwitcher.setInAnimation(fadeIn);
        viewSwitcher.setOutAnimation(fadeout);

        viewPager.setAdapter(adapter);
        circlePageIndicator.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            public boolean isLast = false;
            private Fragment[] scrollFragments = new Fragment[3];
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {



                int size = fragments.size();

                scrollFragments[1] = fragments.get(position);

                if(size>1&&position<size-1){
                    scrollFragments[2] = fragments.get(position+1);
                }else{
                    scrollFragments[2] = null;
                }
                if(position>0){
                    scrollFragments[0] = fragments.get(position-1);
                }else{
                    scrollFragments[0] = null;
                }

                int index = 0;
                for(Fragment fragment : scrollFragments){
                    if(fragment!=null&&fragment instanceof SetupFragment){

                        if(index > 1){
                            positionOffsetPixels = viewPager.getWidth()-positionOffsetPixels;
                        }
                        //Log.d("as","index = "+index+" offset = "+positionOffsetPixels);
                        ((SetupFragment)fragment).onScrolled(positionOffset,positionOffsetPixels);

                    }
                    index++;
                }
            }

            @Override
            public void onPageSelected(int position) {
                if(txtSkip.getVisibility()==View.INVISIBLE){
                    txtSkip.setAlpha(0f);
                    txtSkip.setVisibility(View.VISIBLE);
                    txtSkip.animate().alpha(1f).withLayer().setInterpolator(new DecelerateInterpolator()).setDuration(animationDuration).start();
                }
                if(position==adapter.getCount()-1){
                    txtDone.setText(strDone);
                    viewSwitcher.showNext();

                    isLast = true;
                }else if(isLast){
                    viewSwitcher.showPrevious();
                    isLast = false;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @OnClick(R.id.view_switcher)
    public void nextClick() {

        int currentItem = viewPager.getCurrentItem();
        if(currentItem == adapter.getCount()-1){
            skipClick();
        }else{
            viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
        }
    }

    @OnClick(R.id.txt_skip)
    public void skipClick() {
        Preferences.markSetupDone(this,true);
        setResult(RESULT_OK);
        finish();
    }

    public void onEvent(Object event) {
        if(event instanceof String){
            String langCode = (String)event;
            updateLocale(langCode);


            strDone = getString(R.string.done);

            txtSkip.setText(getResources().getString(R.string.skip));

            for(Fragment fragment : adapter.getItems()){
                if(fragment.isAdded() && fragment instanceof SetupFragment){
                    ((SetupFragment)fragment).bindView();
                }
            }
        }
    }

    private void updateLocale(String langCode) {

        Locale locale = new Locale(langCode);


        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
    }

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity,SetupActivity.class);
        activity.startActivityForResult(intent, REQ_SETUP);
    }
}
