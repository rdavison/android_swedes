package se.chalmers.swedes.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

/**
 * Created by richard on 28/12/15.
 */
public class MooseView extends AnimatedView {



    public MooseView(Context context) {
        super(context);
    }

    public MooseView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MooseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void doDraw(Canvas c) {

    }

    @Override
    protected void updatePhysics(double elapsed) {

    }
}
