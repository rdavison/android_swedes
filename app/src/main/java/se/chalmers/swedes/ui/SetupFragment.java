package se.chalmers.swedes.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.chalmers.swedes.R;

/**
 * Created by richard on 23/12/15.
 */
public abstract class SetupFragment extends Fragment {

    //to deal with binder in subclass
    public static class ViewHolder{
        @Bind(R.id.lay_setup_top)
        FrameLayout layTop;

        @Bind(R.id.lay_setup_bottom)
        FrameLayout layBottom;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }



    public abstract int layoutTopResource();
    public abstract int layoutBottomResource();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_setup,container,false);

        ViewHolder holder = new ViewHolder(root);

        int topIndex = root.indexOfChild(holder.layTop);
        int bottomIndex = root.indexOfChild(holder.layBottom);


        ViewGroup.LayoutParams topParams = holder.layTop.getLayoutParams();
        ViewGroup.LayoutParams bottomParams = holder.layBottom.getLayoutParams();

        root.removeView(holder.layTop);
        root.removeView(holder.layBottom);

        root.addView(inflater.inflate(layoutTopResource(),container,false),topParams);
        root.addView(inflater.inflate(layoutBottomResource(),container,false),bottomParams);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public void bindView(){

    }

    public void onScrolled(float positionOffset, int positionOffsetPixels) {
    }
}
