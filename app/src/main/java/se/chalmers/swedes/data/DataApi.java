package se.chalmers.swedes.data;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;
import se.chalmers.swedes.model.Category;
import se.chalmers.swedes.model.Story;

/**
 * Created by richard on 13/12/15.
 */
public interface DataApi {

    @GET("/refugee/startpage/data.json")
    Observable<List<Story>> startStories();

    @GET("/refugee/story/{id}/data.json")
    Observable<List<Category>> storyCategories(@Path("id") int storyId);
}
