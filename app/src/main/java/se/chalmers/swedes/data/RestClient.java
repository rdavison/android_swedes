package se.chalmers.swedes.data;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.Subscriber;
import se.chalmers.swedes.model.Category;
import se.chalmers.swedes.model.ExplorationItem;
import se.chalmers.swedes.model.Story;
import se.chalmers.swedes.model.TextExplorationItem;

/**
 * Created by richard on 13/12/15.
 */
public class RestClient {


    private static final String CACHE_KEY_START_STORIES = "cache_key_start_stories";
    private static final String CACHE_KEY_CATEGORIES = "cache_key_categories_%d";

    private static final String TAG = RestClient.class.getSimpleName();
    public static final String BASE_URL = "http://vm-05.ituniv.chalmers.se/refugee/";
    private final DataApi service;
    private final DataCache dataCache;
    private final Gson gson;


    private static class Holder {
        static final RestClient INSTANCE = new RestClient();
    }

    public static RestClient getInstance() {
        return Holder.INSTANCE;
    }


    public Gson getGson() {
        return gson;
    }

    private RestClient(){

        dataCache = DataCache.getInstance();


         gson = new GsonBuilder()
                .registerTypeAdapter(ExplorationItem.class, new ExplorationItemDeserializer())
                .create();

        OkHttpClient httpClient = new OkHttpClient.Builder().addNetworkInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Log.d(TAG,"REQUEST URL === "+chain.request().url().toString());

                return chain.proceed(chain.request());
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        service = retrofit.create(DataApi.class);
    }

    public Observable<List<Story>> startStories() {

        return dataCache.getAndSetObservable(CACHE_KEY_START_STORIES,(Class<List<Story>>)(Object)List.class,Story.class,service.startStories());
    }

    public Observable<List<Category>> categories(int storyId){
        return dataCache.getAndSetObservable(String.format(CACHE_KEY_CATEGORIES,storyId),(Class<List<Category>>)(Object)List.class,Category.class,service.storyCategories(storyId));
    }


    public DataApi getService() {
        return service;
    }


    public Observable<File> downloadFile(final String url, final File path) {
        return Observable.create(new Observable.OnSubscribe<File>() {
            @Override
            public void call(Subscriber<? super File> subscriber) {
                try{
                    downloadFile(url,path).toBlocking();
                    subscriber.onNext(path);
                    subscriber.onCompleted();
                }catch (Exception e)
                {
                    subscriber.onError(e);
                }
            }
        });
    }

    public Observable<Integer> downloadFileProgress(final String url, final File path) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {

                InputStream input = null;
                OutputStream output = null;
                HttpURLConnection connection = null;
                int progress = 0;
                try {
                    URL theUrl = new URL(url);
                    Log.d(TAG,"Downloading to "+path.getPath()+" with url:"+url);
                    connection = (HttpURLConnection) theUrl.openConnection();
                    connection.connect();

                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        throw new IOException("Server returned HTTP " + connection.getResponseCode()
                                + " " + connection.getResponseMessage());
                    }
                    int fileLength = connection.getContentLength();

                    path.getParentFile().mkdirs();

                    // download the file
                    input = connection.getInputStream();
                    output = new FileOutputStream(path);

                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1 && !subscriber.isUnsubscribed()) {
                        total += count;
                        //Thread.sleep(100);
                        if (fileLength > 0) {
                            int newProgress = (int) (total * 100 / fileLength);
                            if (newProgress != progress) {
                                progress = newProgress;
                                subscriber.onNext((int) (total * 100 / fileLength));
                            }
                        }
                        output.write(data, 0, count);
                    }
                    output.flush();
                } catch (Exception e) {
                    subscriber.onError(e);
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } catch (IOException ignored) {
                    }

                    if (connection != null)
                        connection.disconnect();

                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                    }
                }

            }
        });
    }

    public class ExplorationItemDeserializer implements JsonDeserializer<ExplorationItem> {
        @Override
        public ExplorationItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonObject jsonObject = json.getAsJsonObject();

            if(jsonObject.has(ExplorationItem.TYPE_MEMBER_NAME) && !jsonObject.get(ExplorationItem.TYPE_MEMBER_NAME).isJsonNull()){

                String type = jsonObject.get(ExplorationItem.TYPE_MEMBER_NAME).getAsString().toLowerCase();
                type = type.substring(0, 1).toUpperCase() + type.substring(1);
                String baseClassName = ExplorationItem.class.getName();
                int lastDot = baseClassName.lastIndexOf('.')+1;
                String className = baseClassName.substring(0,lastDot)+type+baseClassName.substring(lastDot,baseClassName.length());
                try {
                    Class<?> clz = Class.forName(className);
                    return context.deserialize(json, clz);
                } catch (ClassNotFoundException e) {
                    throw new JsonParseException(e);
                }

            }

            return context.deserialize(json, TextExplorationItem.class);
        }


    }
}
