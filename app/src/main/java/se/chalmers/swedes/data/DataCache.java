package se.chalmers.swedes.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * Created by richard on 13/12/15.
 */
public class DataCache {



    private static class Holder {
        static final DataCache INSTANCE = new DataCache();
    }

    public static DataCache getInstance() {
        return Holder.INSTANCE;
    }


    private SharedPreferences prefs;
    private Gson gson = new Gson();


    private DataCache(){

    }

    public void init(Context context){
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /*

    if observable fails we try to retrive the cached version. if that also fails we return an error

     */
    public <T extends Object> Observable<T> getAndSetObservable(final String key, final Class<T> clazz, final Class<?> listClazz,Observable<T> observable) {
        return setObservable(key,observable.onErrorResumeNext(new Func1<Throwable, Observable<T>>() {
            @Override
            public Observable<T> call(Throwable throwable) {
                throwable.printStackTrace();
                return getObservable(key,clazz,listClazz);
            }
        }));
    }
    public <T extends Object> T getAndSet(final String key, final Class<T> clazz, final Class<?> listClazz,T data){
        if(data==null) {
            return get(key, clazz, listClazz);
        }
        set(key,data);
        return data;
    }

    public <T extends Object> Observable<T> getObservable(final String key, final Class<T> clazz, final Class<?> listClazz){
        return Observable.create(new Observable.OnSubscribe<T>() {
            @Override
            public void call(Subscriber<? super T> subscriber) {

                try{
                    T object = get(key,clazz,listClazz);

                    if(object==null) {
                        throw new IllegalStateException("nothing in cache!");
                    }
                    subscriber.onNext(object);
                    subscriber.onCompleted();
                }catch (Exception ex){
                    subscriber.onError(ex);
                }
            }
        });
    }

    public <T extends Object> Observable<T> setObservable(final String key, final Observable<T> observable){

        return observable.flatMap(new Func1<T, Observable<T>>() {
            @Override
            public Observable<T> call(T data) {
                set(key,data);
                return observable;
            }
        });
    }

    @SuppressLint("CommitPrefEdits")
    public boolean set(String key, Object data){

        if(data instanceof File){
            return true;
        }

        try {
            prefs.edit().putString(key, gson.toJson(data)).commit();
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public <T extends Object> T get(String key, Class<T> clazz, Class<?> innerClass) {

        if(clazz.isAssignableFrom(File.class)){
            File file = new File(key);
            if(file.exists()){
                return (T) file;
            }
            return null;
        }



        String json = prefs.getString(key,null);
        if(json==null){
            return null;
        }

        if(innerClass != null){
            Type classType = com.google.gson.internal.$Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, innerClass);
            return gson.fromJson(json,classType);
        }

        return gson.fromJson(json,clazz);
    }
}
