package se.chalmers.swedes.adapter;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import se.chalmers.swedes.model.ParallaxItem;
import se.chalmers.swedes.util.StoryImageHandler;

/**
 * Created by richard on 12/12/15.
 */
public class ParallaxAdapter extends RecyclerView.Adapter<ParallaxAdapter.ViewHolder> {


    private final float scale;
    private final float parallaxScrollFactor;
    private final float adjustedScale;
    private List<ParallaxItem> items = new ArrayList<>(10);
    private final Context context;
    private boolean rightToLeft;


    public ParallaxAdapter(Context context, float scale, float parallaxScrollFactor) {
        this.context = context;
        this.adjustedScale = context.getResources().getDisplayMetrics().widthPixels/(StoryImageHandler.PARALLAX_WIDTH*scale);
        this.scale = scale;
        this.parallaxScrollFactor = parallaxScrollFactor;
    }

    public void setItems(List<ParallaxItem> items) {

        this.items = items;

    }

    public float getParallaxScrollFactor() {
        return parallaxScrollFactor;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams((int)(((StoryImageHandler.PARALLAX_WIDTH*scale*parallaxScrollFactor)/StoryImageHandler.SEGMENT_COUNT)*adjustedScale), ViewGroup.LayoutParams.MATCH_PARENT));

        FrameLayout.LayoutParams fllp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        fllp.gravity = Gravity.BOTTOM;



        rightToLeft = context.getResources().getConfiguration().getLayoutDirection()==ViewCompat.LAYOUT_DIRECTION_RTL;


        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        frameLayout.addView(imageView,fllp);
        return new ViewHolder(frameLayout,imageView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final ParallaxItem item = items.get(position);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) holder.imageView.getLayoutParams();



        params.height = (int) (item.height*scale*adjustedScale);
        params.bottomMargin = item.offset;
        if(rightToLeft) {
            holder.imageView.setScaleX(-1);
        }
        Picasso.with(context).load(item.file).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ViewHolder(FrameLayout frameLayout, ImageView imageView) {
            super(frameLayout);
            this.imageView = imageView;
        }
    }

}
