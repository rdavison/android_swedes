package se.chalmers.swedes.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.chalmers.swedes.R;
import se.chalmers.swedes.SwedesApplication;
import se.chalmers.swedes.model.ExplorationItem;
import se.chalmers.swedes.model.QuizExplorationItem;
import se.chalmers.swedes.model.TextExplorationItem;
import se.chalmers.swedes.model.VideoExplorationItem;

/**
 * Created by richard on 02/01/16.
 */
public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.BaseViewHolder>{

    private static final int VIEW_TYPE_TEXT = 0;
    private static final int VIEW_TYPE_QUIZ = 1;
    private static final int VIEW_TYPE_VIDEO = 2;

    private final Map<String, String> title;
    private List<ExplorationItem> items;

    private LayoutInflater inflater;
    private Context context;
    private int categoryId;

    public ExploreAdapter(Context context, Map<String,String> title, int categoryId){
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.categoryId = categoryId;
        this.title = title;

    }




    @Override
    public int getItemViewType(int position) {

        ExplorationItem item = items.get(position);

        if(item instanceof VideoExplorationItem){
            return VIEW_TYPE_VIDEO;
        }
        if(item instanceof TextExplorationItem){
            return VIEW_TYPE_TEXT;
        }
        if(item instanceof QuizExplorationItem){
            return VIEW_TYPE_QUIZ;
        }


        return 0;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType){
            case VIEW_TYPE_TEXT:
                return new TextViewHolder(inflater.inflate(R.layout.item_stack_card,parent,false));
            case VIEW_TYPE_QUIZ:
                return new QuizViewHolder(inflater.inflate(R.layout.item_stack_quiz,parent,false));
            case VIEW_TYPE_VIDEO:
                return new VideoViewHolder(inflater.inflate(R.layout.item_stack_video,parent,false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {



        ExplorationItem item = items.get(position);

        holder.txtProgress.setText(String.format("%d / %d",position+1,getItemCount()));

        String locale =SwedesApplication.getLocale();

        if(position==0 && item instanceof TextExplorationItem && !(item instanceof VideoExplorationItem)){

            TextViewHolder textViewHolder = (TextViewHolder) holder;
            textViewHolder.imgTop.setImageDrawable(null);
            TextExplorationItem textItem = (TextExplorationItem) item;

            String url = textItem.getImageUrl(categoryId);

            textViewHolder.txtTitle.setText(title.get(locale));

            textViewHolder.txtInfo.setText(textItem.getText().get(locale));

            Picasso.with(context).load(textItem.getImageUrl(categoryId)).into(textViewHolder.imgBottom);
        }else if(item instanceof VideoExplorationItem){
            final VideoViewHolder videoViewHolder = (VideoViewHolder) holder;

            final VideoExplorationItem videoItem = (VideoExplorationItem) item;
            videoViewHolder.videoUrl = videoItem.getVideoUrl(categoryId);
            String imageUrl = videoItem.getImageUrl(categoryId);
            if(imageUrl!=null) {
                Picasso.with(context).load(imageUrl).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        videoViewHolder.imgVideo.setBackground(new BitmapDrawable(context.getResources(), bitmap));
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }
            videoViewHolder.txtTitle.setText(videoItem.getTitle().get(locale));
            videoViewHolder.txtInfo.setText(videoItem.getText().get(locale));
        }else if(item instanceof TextExplorationItem){
            TextViewHolder textViewHolder = (TextViewHolder) holder;
            textViewHolder.imgTop.setImageDrawable(null);

            TextExplorationItem textItem = (TextExplorationItem) item;
            textViewHolder.txtInfo.setText(textItem.getText().get(locale));
            String imageUrl = textItem.getImageUrl(categoryId);
            if(imageUrl!=null) {
                Picasso.with(context).load(imageUrl).into(textViewHolder.imgTop);
            }
        }else if(item instanceof QuizExplorationItem){
            QuizViewHolder quizViewHolder = (QuizViewHolder) holder;

            QuizExplorationItem quizItem = (QuizExplorationItem) item;
            quizViewHolder.txtInfo.setText(quizItem.getText().get(locale));
        }



    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return items==null?0:items.size();
    }


    public void setItems(List<ExplorationItem> items) {
        this.items = items;



        //notifyDataSetChanged();
    }

    public static class BaseViewHolder extends RecyclerView.ViewHolder{


        public TextView txtProgress;

        public BaseViewHolder(View view) {
            super(view);

            txtProgress = (TextView) view.findViewById(R.id.txt_progress);


        }
    }

    public static class TextViewHolder extends BaseViewHolder{

        @Bind(R.id.txt_info)
        TextView txtInfo;

        @Bind(R.id.txt_title)
        TextView txtTitle;

        @Bind(R.id.img_top)
        ImageView imgTop;

        @Bind(R.id.img_bottom)
        ImageView imgBottom;

        public TextViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);


        }
    }

    public static class QuizViewHolder extends BaseViewHolder {


        @Bind(R.id.txt_info)
        TextView txtInfo;



        public QuizViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public static class VideoViewHolder extends BaseViewHolder {

        @Bind(R.id.img_video)
        ImageView imgVideo;


        @Bind(R.id.txt_info)
        TextView txtInfo;

        @Bind(R.id.txt_title)
        TextView txtTitle;

        String videoUrl;

        public VideoViewHolder(final View view) {
            super(view);
            ButterKnife.bind(this,view);

            imgVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Log.d("ASfasf",VideoViewHolder.this.videoUrl);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(VideoViewHolder.this.videoUrl), "video/mp4");
                    view.getContext().startActivity(intent);
                }
            });
        }
    }
}
