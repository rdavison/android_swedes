package se.chalmers.swedes.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.chalmers.swedes.R;
import se.chalmers.swedes.SwedesApplication;
import se.chalmers.swedes.model.Category;

/**
 * Created by richard on 07/01/16.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private AdapterView.OnItemClickListener onItemClickListener;
    private final LayoutInflater inflater;
    private final Context context;
    private final String strCards;

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private final List<Category> categories;

    public CategoryAdapter(Context context, List<Category> categories) {

        this.strCards = context.getString(R.string.cards);
        this.context = context;
        this.categories = categories;
        inflater = LayoutInflater.from(context);
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.item_category,parent,false),onItemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Category category = categories.get(position);

        holder.txtTitle.setText(category.getTitle().get(SwedesApplication.getLocale()));
        holder.txtCount.setText(category.getItemSize()+" "+strCards);
        String imageUrl = category.getImageUrl();
        if(imageUrl!=null) {
            Picasso.with(context).load(imageUrl).into(holder.imgCategory);
        }
    }

    @Override
    public int getItemCount() {
        return categories==null?0:categories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.txt_count)
        TextView txtCount;

        @Bind(R.id.txt_title)
        TextView txtTitle;

        @Bind(R.id.txt_subtitle)
        TextView txtSubTitle;

        @Bind(R.id.img_category)
        ImageView imgCategory;

        public ViewHolder(View itemView, final AdapterView.OnItemClickListener onItemClickListener) {
            super(itemView);

            ButterKnife.bind(this,itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(null,v,getAdapterPosition(),getAdapterPosition());
                }
            });
        }
    }
}
