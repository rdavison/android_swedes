package se.chalmers.swedes.adapter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import se.chalmers.swedes.SwedesApplication;
import se.chalmers.swedes.R;
import se.chalmers.swedes.model.Story;
import se.chalmers.swedes.ui.widget.TextFloatingActionButton;

/**
 * Created by richard on 09/12/15.
 */
public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder> {

    public static interface OnItemClickListener{
        void onClick(int position);
    }



    //private static final int STORIES_PER_PARALLAX = 1;
    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_SPACE = 0;



    private final List<Story> items;
    private final LayoutInflater layoutInflator;
    private final int width;
    private final Context context;

    private OnItemClickListener itemClickListener;

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public StoryAdapter(Context context, List<Story> items) {
        this.context = context;
        this.layoutInflator = LayoutInflater.from(context);
        Point point = new Point();
        ((WindowManager)context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(point);
        this.width = point.x;//Utils.dpToPx(context, StoryImageHandler.PARALLAX_WIDTH);
        this.items = items;
    }

    public int getWidth() {
        return width;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(layoutInflator.inflate(R.layout.item_start_story,parent,false),width,itemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int viewType = getItemViewType(position);

        if(viewType==VIEW_TYPE_SPACE){
            holder.btnExplore.setVisibility(View.GONE);
            holder.txtInfo.setVisibility(View.GONE);
            //holder.txtTitle.setVisibility(View.GONE);
        }else{

            Story item = items.get(position);

            holder.btnExplore.setVisibility(View.VISIBLE);
            holder.txtInfo.setVisibility(View.GONE);
            holder.txtInfo.setText(item.getDescription().get(SwedesApplication.getLocale()));
            holder.btnExplore.setText(item.getTitle().get(SwedesApplication.getLocale()));

            Picasso.with(context).load(item.getImages().getMainImageFile()).into(holder.imgStory);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private final OnItemClickListener listener;
        @Bind(R.id.txt_info)
        TextView txtInfo;
        @Bind(R.id.btn_explore)
        TextFloatingActionButton btnExplore;

        @Bind(R.id.img_story)
        ImageView imgStory;

        public ViewHolder(View itemView, int width, OnItemClickListener listener) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            btnExplore.setOnClickListener(this);

            this.listener = listener;

            itemView.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
        }

        @Override
        public void onClick(View v) {
            listener.onClick(getAdapterPosition());
        }
    }
}
