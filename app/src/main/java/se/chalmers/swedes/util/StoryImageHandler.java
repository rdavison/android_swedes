package se.chalmers.swedes.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.FuncN;
import se.chalmers.swedes.data.RestClient;
import se.chalmers.swedes.model.ParallaxItem;
import se.chalmers.swedes.model.Story;
import se.chalmers.swedes.model.StoryImages;

/**
 * Created by richard on 14/12/15.
 */
public class StoryImageHandler {


    public static final int IMAGE_DENSITY = 3;
    public static final int PARALLAX_WIDTH = 1080;
    public static final int SEGMENT_COUNT = 4;

    private static final String TAG = StoryImageHandler.class.getSimpleName();
    private static final String STORY_IMAGE_BASE_URL = RestClient.BASE_URL + "startpage/images/%d/%s";
    private static final String STORY_IMAGE_PATH = "%s/story_images/%d/";
    private static final String MAIN_NAME = "main.png";

    private static final String IMAGE_NAME_SOURCE = "_raw.png";
    private static final String IMAGE_NAME_SEGMENT = ".png";

    private final RestClient restClient;
    private final File cacheDir;
    private final BitmapFactory.Options options = new BitmapFactory.Options();
    private final EventBus<DownloadProgress> progressEventBus;

    private int totalProgress = 0;
    private int totalImageCount = 0;

    public static class  DownloadProgress{
        public int progress;

        public DownloadProgress(int progress) {
            this.progress = progress;
        }
    }

    public StoryImageHandler(RestClient restClient, File cacheDir,EventBus<DownloadProgress> progressEventBus) {
        this.restClient = restClient;
        this.cacheDir = cacheDir;
        this.progressEventBus = progressEventBus;

        options.inDither = true;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
    }

    public Observable<Void> downloadWithProgressObservable(final String url, final File target) {

        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {

                try {
                    restClient.downloadFileProgress(url, target).toBlocking().forEach(new Action1<Integer>() {

                        int lastProgress = 0;

                        @Override
                        public void call(Integer progress) {
                            //TODO post progress to event bus

                            int deltaProgress = progress-lastProgress;

                            totalProgress+=deltaProgress;

                            lastProgress = progress;

                            if(progress==100){
                                totalProgress += 100/totalImageCount;
                            }


                            Log.d(TAG,"TOTAL PROCESS "+totalProgress + "total image count = "+totalImageCount);

                            progressEventBus.post(new DownloadProgress(totalProgress/(totalImageCount+1)));

                            Log.d(TAG, "Download image file: " + url + " " + progress + "%");
                        }
                    });
                    subscriber.onNext(null);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });

    }

    public Observable<Void> handleImages(List<Story> stories, final float scale) {

        List<Observable<Void>> parallelObservables = new ArrayList<>(10);

        totalImageCount = stories.size();


        int storyIndex = 0;
        for (Story story : stories) {



            final StoryImages images = story.getImages();
            final File rootImagePath = new File(String.format(STORY_IMAGE_PATH, cacheDir.getPath(), storyIndex));
            final String storyImagePath = rootImagePath.getPath() + "/%d/%s";
            final String mainImageUrl = String.format(STORY_IMAGE_BASE_URL, storyIndex, images.getMainUrl());
            final File mainImageFile = new File(rootImagePath, MAIN_NAME);

            if(storyIndex == 0){
                totalImageCount *= images.getParallax().size()+1;
            }


            //TODO handle revisions


            //if mainfile exists
            if (!mainImageFile.exists()) {
                parallelObservables.add(downloadWithProgressObservable(mainImageUrl, mainImageFile).doOnCompleted(new Action0() {
                    @Override
                    public void call() {
                        images.setMainImageFile(mainImageFile);
                    }
                }));
            } else {
                images.setMainImageFile(mainImageFile);
            }

            int i = 0;
            for (String key : images.getParallax().keySet()) {


                List<String> parallaxAtLevel = images.getParallax().get(key);

                images.getParallaxItems().put(i, new SparseArrayCompat<ParallaxItem>(10));

                final int offset = (int) (Float.parseFloat(parallaxAtLevel.remove(0)) * scale);

                for (int loop = 0; loop < parallaxAtLevel.size(); loop++) {
                    String temp = parallaxAtLevel.get(loop);
                    if (temp == null) {
                        parallaxAtLevel.remove(loop);
                    }
                }


                int ii = 0;
                for (final String image : parallaxAtLevel) {
                    final int zIndex = i, partIndex = ii;


                    //String cacheName = String.format(CACHE_KEY_FILE,index,zIndex,loop1);
                    String imageUrl = String.format(STORY_IMAGE_BASE_URL, storyIndex, image);
                    final File imageFile = new File(String.format(storyImagePath, i, ii + IMAGE_NAME_SOURCE));


                    int targetSegmentSize = parallaxAtLevel.size() * SEGMENT_COUNT;

                    if (!imageFile.getParentFile().exists() || imageFile.getParentFile().listFiles().length < targetSegmentSize) {

                        options.inJustDecodeBounds = false;

                        parallelObservables.add(downloadWithProgressObservable(imageUrl, imageFile).takeLast(0).concatWith(Observable.create(new Observable.OnSubscribe<Void>() {
                            @Override
                            public void call(Subscriber<? super Void> subscriber) {

                                Log.d(TAG, "===== EXISTS????====" + imageFile.getPath() + imageFile.exists());

                                try {

                                    Bitmap sourceBitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
                                    if (sourceBitmap == null) {
                                        subscriber.onError(new IOException("Could not decode :" + imageFile.getPath()));
                                    } else {

                                        for (int segmentIndex = 0; segmentIndex < SEGMENT_COUNT; segmentIndex++) {

                                            int sampleWidth = options.outWidth / SEGMENT_COUNT;

                                            int segmentNo = (segmentIndex + (partIndex * SEGMENT_COUNT));
                                            File targetFile = new File(String.format(storyImagePath, zIndex, segmentNo + IMAGE_NAME_SEGMENT));
                                            Bitmap bitmap2 = Bitmap.createBitmap(sourceBitmap, sampleWidth * segmentIndex, 0, sampleWidth, options.outHeight);
                                            Bitmap bitmap3 = null;
                                            int width = (int) ((float) sampleWidth * scale);
                                            int height = (int) ((float) options.outHeight * scale);
                                            if (scale != 1) {
                                                bitmap3 = Bitmap.createScaledBitmap(bitmap2, width, height, true);
                                                bitmap2.recycle();
                                            } else {
                                                bitmap3 = bitmap2;
                                            }
                                            FileOutputStream fos = new FileOutputStream(targetFile);
                                            bitmap3.compress(Bitmap.CompressFormat.PNG, 90, fos);
                                            fos.flush();
                                            fos.close();
                                            bitmap3.recycle();
                                            appendParallaxWithFile(images.getParallaxItems().get(zIndex), segmentNo, targetFile, offset, scale, width, height);
                                        }
                                    }
                                    sourceBitmap.recycle();
                                    imageFile.delete();
                                    subscriber.onNext(null);
                                    subscriber.onCompleted();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                    subscriber.onError(e);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    subscriber.onError(e);
                                }


                            }
                        })).doOnCompleted(new Action0() {
                            @Override
                            public void call() {
                                Log.d(TAG, "Image file cropped successfully");
                            }
                        }));
                    } else if (images.getParallaxItems().get(zIndex).size() < targetSegmentSize) {
                        options.inJustDecodeBounds = true;
                        int segmentIndex = 0;
                        for (File sourceFile : imageFile.getParentFile().listFiles()) {
                            Bitmap sourceBitmap = BitmapFactory.decodeFile(sourceFile.getPath(), options);
                            appendParallaxWithFile(images.getParallaxItems().get(zIndex), segmentIndex, sourceFile, offset, scale, options.outWidth, options.outHeight);
                            segmentIndex++;
                        }
                    }
                    ii++;
                }
                i++;
            }

            storyIndex++;
        }

        if (parallelObservables.size() == 0) {
            return Observable.just(null);
        }

        return Observable.zip(parallelObservables, new FuncN<Void>() {
            @Override
            public Void call(Object... args) {
                return null;
            }
        });
    }

    private void appendParallaxWithFile(SparseArrayCompat<ParallaxItem> parallaxItems, int segmentIndex, File targetFile, int offset, float scale, int width, int height) {

        parallaxItems.put(segmentIndex, new ParallaxItem(targetFile, offset, scale, (int) (width * (1f / scale)), (int) (height * (1f / scale))));

    }
}
