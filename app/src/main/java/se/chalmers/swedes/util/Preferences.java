package se.chalmers.swedes.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by richard on 21/12/15.
 */
public class Preferences {


    private static final String PREF_KEY_SETUP_DONE = "pref_key_setup_done";
    private static SharedPreferences prefs;

    public static boolean setupDone(Context context) {
        checkPrefs(context);
        return prefs.getBoolean(PREF_KEY_SETUP_DONE,false);
    }

    public static boolean markSetupDone(Context context,boolean done) {
        checkPrefs(context);
        return prefs.edit().putBoolean(PREF_KEY_SETUP_DONE,done).commit();
    }

    private static void checkPrefs(Context context) {
        if(prefs==null) {
            prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        }
    }
}
