package se.chalmers.swedes.util;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

/**
 * Created by richard on 15/12/15.
 */
public class SplitBackgroundDrawable extends Drawable {

    private int colorTop;
    private int colorBottom;

    private Paint[] paints = new Paint[2];

    public SplitBackgroundDrawable(int colorTop, int colorBottom) {
        this.colorTop = colorTop;
        this.colorBottom = colorBottom;

        paints[0] = new Paint();
        paints[0].setColor(colorTop);

        paints[1] = new Paint();
        paints[1].setColor(colorBottom);


    }

    @Override
    public void draw(Canvas canvas) {

        Rect b = getBounds();

        int i =0;
        int halfHeight = b.height()/2;
        for(Paint p : paints){
            Rect rect = new Rect(b);
            if(i == 0){
                rect.bottom =halfHeight;
            }else{
                rect.top = halfHeight;
            }
            canvas.drawRect(rect,p);
            i++;
        }
    }

    public void setColors(int colorTop, int colorBottom){
        this.colorBottom = colorBottom;
        this.colorTop = colorTop;

        paints[0].setColor(colorTop);
        paints[1].setColor(colorBottom);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 100;
    }
}
