package se.chalmers.swedes.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by richard on 19/12/15.
 */
public class SimpleFragmentPagerAdapter extends FragmentStatePagerAdapter {


    private final List<Fragment> items;


    public SimpleFragmentPagerAdapter(FragmentManager fm, List<Fragment> items) {
        super(fm);
        this.items = items;
    }

    @Override
    public Fragment getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public List<Fragment> getItems() {
        return items;
    }
}
