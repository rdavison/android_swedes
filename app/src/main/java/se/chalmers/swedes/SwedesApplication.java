package se.chalmers.swedes;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Locale;

import se.chalmers.swedes.data.DataCache;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by richard on 13/12/15.
 */
public class SwedesApplication extends Application {

    private static final String PREF_KEY_LOCALE = "pref_key_locale";

    private static String locale;

    private static SharedPreferences prefs;

    private static Application application;

    @Override
    public void onCreate() {
        super.onCreate();


        application = this;
        prefs = PreferenceManager.getDefaultSharedPreferences(application);
        locale = prefs.getString(PREF_KEY_LOCALE,Locale.getDefault().getLanguage());

        DataCache.getInstance().init(application.getApplicationContext());

    }

    public static String getLocale() {
        return locale;
    }

    public static void setLocale(String locale) {
        SwedesApplication.locale = locale;
        prefs.edit().putString(PREF_KEY_LOCALE,locale).commit();

    }
}
