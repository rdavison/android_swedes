package android.widget;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by richard on 02/01/16.
 */
public class VerticalStackView extends AdapterViewAnimator {
    private final Context context;

    public VerticalStackView(Context context) {
        this(context,null);
    }

    public VerticalStackView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public VerticalStackView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;
    }



    /*@Override
    FrameLayout getFrameForChild() {
        return new FrameLayout(context);
    }*/




}
